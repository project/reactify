INTRODUCTION
------------

React and Redux powered theme providing decoupled architecture.


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

 * Reactify theme utilities (https://www.drupal.org/project/reactify_utilities)
   Provides necessary functionality and examples for Reactify theme integration.
   Adds REST endpoints, authentication, CRUD endpoints for users, content and
   comments.
   Provides example of implementing custom entities, what would be suitable for
   web applications.


INSTALLATION
------------

 * Install Reactify theme:  drush dl reactify -> drush en reactify -y

 * Install Reactify Theme Utilities module: drush dl reactify_utilities -> drush
   en reactify_utilities -y
   The module contains several submodules for authentication and REST endpoints
   configuration.

 * Uninstall core modules Quick Edit and Contextual links. The reason is that
   those modules are not used on theme's
   frontend, but they are enabled by default and they have jQuery dependency.
   Reactify theme disables core libraries, so jQuery, jQuery Once etc are not
   loaded. There will be just white screen and console error 'jQuery is not
   defined' if modules would be enabled.

 * (Optional) Install Devel module, enable Devel Generate module: drush dl
   devel, drush en devel_generate -y
   Some content could be generated to show theme's functionality

 * Download JSON Web Token Authentication module:
   https://www.drupal.org/project/jwt
   Please consider to download the module via composer: go to site's root folder
   and type 'composer require drupal/jwt'.

   JWT module has library dependancy (firebase/php-jwt) and installation via
   composer ensures that library would be added as well.
   JWT module uses Key module for storing secret key.
   Instructions can be found on Key module's page:
   https://www.drupal.org/project/key


CONFIGURATION
-------------

Theme provides settings page available at /admin/appearance/settings/reactify

Available settings:

 * Full width - indicates whether to use full-width layout
 * Front page - provides select list of existing nodes to set as front page
 * About page - provides select list of existing nodes to set as about us page
 * Banner block - provides select list of content block ids to set as front page
   banner
 * Layout options - provides layout configuration. Allows to hide header,
   footer, sidebar, banner and set sidebar position
 * Theme text - configuration of footer text
 * Dashboard settings - displays dashboard settings in admin area, configuration
   available if 'Dashboard statistics' submodule is enabled in Reactify theme
   utilities module.


LOCAL DEVELOPMENT
-----------------

React components and build tools are located in 'react' folder.
Enter 'react' folder and install node modules necessary for theme
and development by running 'npm i'.

Theme uses Webpack for development and build processes.
There are 2 webpack configuration files for development and
 production build.

For development run 'npm run dev' and theme will be available on
the address 'http://localhost:8080' with hot reloading enabled.
It uses webpack.dev.config.js

For building run 'npm run build' and it will use
webpack.prod.config.js. Created files are called
bundle.js and bundle.css and files will be placed to 'build'
folder in theme root. Theme's libraries.yml file defines
created files as theme's assets.

Reactify uses 'reactify_utilities' module for creation of theme settings,
REST endpoints and authentication.

Even though Drupal 8 provides good foundation for REST services,
some features still were added.
Module can serve as example for building custom functionality.


THEME STRUCTURE
---------------

Compiled React app is located in 'build' folder and is attached as a library
in theme info.yml file
App's source code lives in 'src' folder and structured in various folders
for further maintainability.

Theme uses Redux for state management and React router for routing.
Theme was designed as single page app, though it excludes paths to
'/user*' and '/admin*' sections for accessing Drupal backend.
This is relevant for projects using theme on the same domain as Drupal's
backend.

It can be used as well on another domains. In that case it would need only
'build' folder and some .html file for mounting app.
Also, backend's url should be specified in 'src/config.js' file for
REST requests to backend.
By default there is empty string and, thus, theme requests endpoints on the
same domain.

Reactify has public and protected areas. Protected area is available after
signing in on /dashboard* urls.

Reactify has multilingual support for theme interface. Backend REST endpoints
should be configured separately.


MAINTAINERS
-----------

Oleg Elizarov (olegel) - https://www.drupal.org/u/olegel
