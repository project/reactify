import { setLang } from './index'
import { SET_LANG } from '../constants';

describe('actions', () => {
  it('should create an action setting language', () => {
    const lang = 'en'
    const expectedAction = {
      type: SET_LANG,
      lang
    }
    expect(setLang('en')).toEqual(expectedAction)
  })
})
