import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import { faCogs, faBell, faBellSlash } from '@fortawesome/fontawesome-free-solid'
import ProfileCard from '../Profile/ProfileCard'
import LangSwitcher from '../LangSwitcher'
import './style.scss'

class AdminProfileMenu extends Component {
  render() {
    const {user, notificationBar, toggleNotifications, changeLanguage, language} = this.props
    return (
      <nav className="auth-profile-nav">
        <LangSwitcher language={language} changeLanguage={changeLanguage} />
        <span className="auth--notification-icon" onClick={() => toggleNotifications()}><FontAwesomeIcon icon={notificationBar ? faBellSlash : faBell} /></span>
        <ProfileCard user={user.user} />
        <NavLink to="/dashboard/settings" className="auth--settings-link">
          <FontAwesomeIcon icon={faCogs} />
          <span className="tooltip">Settings</span>
        </NavLink>
      </nav>
    )
  }
}

export default AdminProfileMenu
